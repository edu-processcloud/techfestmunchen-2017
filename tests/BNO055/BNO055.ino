#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <Adafruit_LSM303_U.h>


/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

void displaySensorDetails(void)
{
  sensor_t sensor;
  accel.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" m/s^2");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}



/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (100)

Adafruit_BNO055 bno = Adafruit_BNO055();

int x, _x, xstatic = 0;
int y, _y, ystatic = 0;
int z, _z, zstatic = 0;

/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/
void setup(void)
{
  Serial.begin(9600);


  Serial.println("Starting Accelerometer Sensor"); Serial.println("");

  /* Initialise the sensor */
  if(!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }

  /* Display some basic information on this sensor */
  displaySensorDetails();



  Serial.println("Starting Position Sensor"); Serial.println("");


  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  sensors_event_t event;
  accel.getEvent(&event);

  xstatic = event.acceleration.x;
  ystatic = event.acceleration.y;
  zstatic = event.acceleration.z;


  delay(1000);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");

  bno.setExtCrystalUse(true);

  Serial.println("Calibration status values: 0=uncalibrated, 3=fully calibrated");
}

void loop(void)
{

  /* Get a new sensor event */
  sensors_event_t event;
  accel.getEvent(&event);

  if(event.acceleration.x > 1 ){
    Serial.print("X acc: "); Serial.println(event.acceleration.x);
    }
  if(event.acceleration.y > 1 ){
    Serial.print("Y acc: "); Serial.println(event.acceleration.y);
    }
 /* if(event.acceleration.z > 1 ){
    Serial.print("Z acc: "); Serial.println(event.acceleration.z);
    }
*/
  
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

  _x = euler.x();
  _y = euler.y();
  _z = euler.z();

  if( x != _x || y != _y || z != _z ){


  Serial.print("X acc: "); Serial.print(event.acceleration.x); Serial.print("  pos: "); Serial.println(euler.x());
  Serial.print("Y acc: "); Serial.print(event.acceleration.y); Serial.print("  pos  "); Serial.println(euler.y());
  Serial.print("Z acc: "); Serial.print(event.acceleration.z); Serial.print("  pos  "); Serial.println(euler.y());

  Serial.println("");
  x = euler.x();
  y = euler.y();
  z = euler.z();


  }
  delay(BNO055_SAMPLERATE_DELAY_MS);
}
